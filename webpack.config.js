var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var SpritesmithPlugin = require('webpack-spritesmith');
var webpackUglifyJsPlugin = require('webpack-uglify-js-plugin');
var path = require('path');
 
module.exports = {
  context: __dirname+'/dev',
  entry: './js/index.js',
  output: {
    path: __dirname+'/public/',
    filename: 'js/bundle.js',
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [{
      test: /\.pug$/,
      loader: 'pug-html-loader'
    },{
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract('css!postcss-loader?parser=postcss-scss')
    },{
      test: /\.(jpe?g|gif|png|svg)$/,
      loader: 'file-loader?name=[path][name].[ext]'
    },{
      test: /\.(json)$/,
      loader: 'json-loader'
    },{
      test: /\.jsx?$/,
      exclude: [/node_modules/],
      loader: "babel-loader",
      query: {
        presets: ['es2015', 'react', 'stage-0', 'stage-1']
      }
    }]
  },
  resolve: {
    modulesDirectories: ["web_modules", "node_modules", "spritesmith-generated"]
  },
  postcss: function() {
    return [
        require('postcss-strip-inline-comments'),
        require('postcss-smart-import')({
          addDependencyTo: webpack
        }),
        require('precss'),
        require('postcss-extend'),
        require('postcss-short'),
        require('postcss-functions')({
          functions: {
            em: function (value, size) {
              if(size === undefined) size = 16;
              value = parseInt(value);
              size = parseInt(size);
              if(value % size) {
                return(value / size).toFixed(3) + 'em';
              } else {
                return(value / size).toFixed(0) + 'em';
              }
            },
          }
        }),
        require('postcss-url'),
        require("postcss-cssnext")({
          browsers: ['> 1%', 'last 5 versions', 'IE 7']
        }),
        require('postcss-browser-reporter'),
        require('postcss-reporter'),
        require('postcss-short'),
        require("cssnano"),
        require("css-mqpacker")
    ];
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: './pug/index.pug'
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/1_tv.html",
      template: './pug/slider_data/1_tv.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/5_tv.html",
      template: './pug/slider_data/5_tv.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/billboard.html",
      template: './pug/slider_data/billboard.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/column.html",
      template: './pug/slider_data/column.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/dance_radio.html",
      template: './pug/slider_data/dance_radio.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/hit_radio.html",
      template: './pug/slider_data/hit_radio.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/love_radio.html",
      template: './pug/slider_data/love_radio.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/ntv_tv.html",
      template: './pug/slider_data/ntv_tv.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/r1_tv.html",
      template: './pug/slider_data/r1_tv.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/road_radio.html",
      template: './pug/slider_data/road_radio.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/rock_radio.html",
      template: './pug/slider_data/rock_radio.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/rr_radio.html",
      template: './pug/slider_data/rr_radio.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/shanson_radio.html",
      template: './pug/slider_data/shanson_radio.pug',
      inject: false,
    }),
    new HtmlWebpackPlugin({
      filename: "slider_data/tvc_tv.html",
      template: './pug/slider_data/tvc_tv.pug',
      inject: false,
    }),
    new ExtractTextPlugin('style.css'),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
    }),
    new webpackUglifyJsPlugin({
      cacheFolder: path.resolve(__dirname, 'public/js/'),
      debug: true,
      minimize: true,
      sourceMap: false,
      output: {
          comments: false
        },
      compressor: {
          warnings: false
        }
    }),
    new SpritesmithPlugin({
      src: {
        cwd: path.resolve(__dirname, 'dev/sprite/img'),
        glob: '*'
      },
      target: {
        image: path.resolve(__dirname, 'dev/img/sprite.png'),
        css: [[path.resolve(__dirname, 'dev/pcss/_sprite.scss'),{
          format: 'tmp'
        }]]
      },
      spritesmithOptions: {
        algorithm: 'binary-tree',
        padding: 4,
      },
      customTemplates: {
        'tmp': path.resolve(__dirname,'dev/sprite/temp.handlebars')
      },
      apiOptions: {
        cssImageRef: "~sprite.png"
      }
    })
  ],
  devServer: {
    inline: true,
    hot: true,
    contentBase: "./public",
  }
};



