import '../img/love_radio.png';
import '../img/love_radio_img.jpg';

let linksArr = [
  '1_tv.html',
  'r1_tv.html',
  'ntv_tv.html',
  'tvc_tv.html',
  '5_tv.html',
  'love_radio.html',
  'rock_radio.html',
  'rr_radio.html',
  'road_radio.html',
  'shanson_radio.html',
  'hit_radio.html',
  'dance_radio.html',
  'billboard.html',
  'column.html',
];

export default function (item) {
  let url = `./slider_data/${linksArr[item]}`;

  $.ajax({
    url: url,
    success: function(res) {
      let wrapper = document.createElement('div');
      wrapper.setAttribute('class','wrapper');
      wrapper.innerHTML = res;
      $('.blockCarries .slider__container').html(wrapper);
    }
  });
}

