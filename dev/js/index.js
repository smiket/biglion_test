import '../pcss/style.scss';
import LoadSlide from './_slider.js';
import animateGraph from './_graph.js';


let stat = document.querySelector('.blockStats');

// graphAnimate
window.onscroll = function() {
  if (this.pageYOffset+this.innerHeight/2 >= stat.offsetTop) {
    $('.graph__bar').each(function(){
      animateGraph(this);
    });
  }
};


$('.header .menu__button').click(function(){
  $('.header__menu').toggleClass('active');
});

$('.scrollto').click(function(e) {
  var anchor = $(this).attr('href');
  $('html, body').stop().animate({
    scrollTop: $(anchor).offset().top
  }, 1000);
  e.preventDefault();
});


$('.blockCarries .ic').click(function(){
      $('.blockCarries .slide__links .ic').removeClass('active');
      $(this).addClass('active');
  LoadSlide(this.getAttribute('data-href'));
});



ymaps.ready(initMap);

function initMap() {

  var myGeocoder = ymaps.geocode('Калуга');
  myGeocoder.then(
    function(res) {
      var pos = res.geoObjects.get(0).geometry.getCoordinates();
      var myMap = new ymaps.Map('map', {
        center: pos,
        zoom: 17
      });

      var placemark = new ymaps.Placemark([pos[0],pos[1]], {
        balloonContentHeader: '<p>Калуга</p>'
      });

      myMap.geoObjects.add(placemark);
      myMap.controls
        .add('zoomControl')
        .add('typeSelector')
        .add('mapTools');

      myMap.events.add('click', function(e) { myMap.balloon.close(); });

    }
  );

};

