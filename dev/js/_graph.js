  export default function(item) {
    if (item.hasAttribute('style')) return false;

    let el = item.previousSibling;
    let percent = el.getAttribute('data-percent');
    let i = percent;


    (function animateText(){
      if (i<0) return percent;

      item.style.strokeDasharray = ((percent-i)*6.3).toFixed(0) + ' 630';
      el.innerHTML = percent - i;
      i--;
      setTimeout(animateText,1000/percent);
    })();
  }
